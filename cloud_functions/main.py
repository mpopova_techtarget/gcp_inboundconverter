"""
main.py contains the main processing logic for processing
"""
import json
import copy
import os
import base64

import requests
from google.cloud import pubsub_v1, bigquery
from lib.rules.general import process_rules, filter_columns
from lib.rules.config import get_config, get_web_scraper_config, get_inbound_converter_config
from lib.rules.page_content import page_content
from lib.utilities.error_handler import append_error

CONFIG = os.getenv('CONFIG')
project = os.getenv("PROJECT")
EDC_REST_URL = os.getenv("EDC_REST_URL")


def write_to_bq(event, context):
    # pylint: disable=unused-argument
    """
    Cloud Function to load to BQ
    :param event: (dict) Dictionary that contains both the configuration and the post-processed data from IBC.
    :param context: (google.cloud.functions.Context) The Cloud Functions event
         metadata. The `event_id` field contains the Pub/Sub message ID. The
         `timestamp` field contains the publish time
    :return: None
    """

    data = json.loads(base64.b64decode(event['data']).decode('utf-8'))
    bq_client = bigquery.Client()
    config = get_config(CONFIG)
    tables = config['load'].get('tables', [])
    output = copy.deepcopy(data)
    for table in tables:
        if table.get("enabled", True):
            load_data = []
            for rec in output:
                output_data = copy.deepcopy(rec)
                if table.get("rules"):
                    output_data = process_rules(config=table, data=output_data)
                filtered_rec = filter_columns(data=output_data, columns=table.get('columns'))
                if filtered_rec:
                    load_data.append(filtered_rec)

            if load_data:
                try:
                    errors = bq_client.insert_rows_json(table['table_name'].format(project), load_data)

                    if errors:
                        for data in load_data:
                            # may have to loop over the indicies of the return to get the actual rows that errored?
                            append_error(data, errors, description="Could not write to {}".format(table['table_name']))

                        publish_error_to_pubsub(load_data)

                except Exception as e:
                    for data in load_data:
                        append_error(data, json.dumps([str(e)]),
                                     "Fatal Error occurred writing to {}".format(table['table_name']))

                    publish_error_to_pubsub(load_data)


def publish_error_to_pubsub(load_data):
    """
    TODO get rid of the config - just send the data to the failed events topic
    """
    error_config = {
        "postprocess": [
            {"topic": "ibc.failed-events"}
        ]
    }
    publish_list_to_pubsub(output_list=load_data, config=error_config)


def handle_failed_event(event, context):
    # pylint: disable=unused-argument
    """
    Cloud Function to load to BQ
    :param event: (dict) Dictionary that contains both the configuration and the post-processed data from IBC.
    :param context: (google.cloud.functions.Context) The Cloud Functions event
         metadata. The `event_id` field contains the Pub/Sub message ID. The
         `timestamp` field contains the publish time
    :return: None
    """

    data = json.loads(base64.b64decode(event['data']).decode('utf-8'))
    bq_client = bigquery.Client()
    config = get_config(CONFIG)
    tables = config['load'].get('tables', [])
    output = copy.deepcopy(data)
    for table in tables:
        if table.get("enabled", True):
            load_data = []
            for rec in output:
                output_data = copy.deepcopy(rec)
                if table.get("rules"):
                    output_data = process_rules(config=table, data=rec)
                filtered_rec = filter_columns(data=output_data, columns=table.get('columns'))
                if filtered_rec != {}:
                    filtered_rec["ERRORS"] = json.dumps(filtered_rec["ERRORS"])
                    load_data.append(filtered_rec)

            if load_data:
                print('about to load data into table: ' + table['table_name'].format(project) + '; data: ' +
                      json.dumps(load_data))
                errors = bq_client.insert_rows_json(table['table_name'].format(project), load_data)

                if errors:
                    print('Failed to insert data for a failed event into BigQuery: ' + table['table_name'].format(
                        project) + '-' +
                          json.dumps(load_data) + '-' + json.dumps(errors))


def process_event(event, context):
    """
    Main procesing logic to receive data from pub/sub, apply rules to data, and load to BQ
    :param event: (dict) The dictionary with data specific to this type of
         event. The `data` field contains the PubsubMessage message. The
         `attributes` field will contain custom attributes if there are any.
    :param context: (google.cloud.functions.Context) The Cloud Functions event
         metadata. The `event_id` field contains the Pub/Sub message ID. The
         `timestamp` field contains the publish time
    :return: (dict) post-processed data dictionary
    """

    config = get_inbound_converter_config()
    failed_events_topic = config['failed_events_topic']
    try:
        if config['data_tag'] == 'attributes':
            data = json.loads(json.dumps(event['attributes']))
        elif config['data_tag'] == 'data':
            data = json.loads(base64.b64decode(event['data']).decode('utf-8'))
        else:
            raise Exception('No Data tag found in config')

        messages = []
        if type(data) == list:
            messages = data
        else:
            messages.append(data)

        output_messages_list = apply_config_rules(context=context, messages=messages, config=config)

        result_messages_list = []
        for message in output_messages_list:
            # if there are any errors happened during the apply_config_rules() execution -
            # send event to the failed events topic and continue with the next event (if any)
            if message.get('ERRORS', []):
                print("There were errors processing event:{} - sending it to the {} topic: ".format(
                    json.dumps(message), failed_events_topic
                ))
                result_messages_list.append(message)  # this is for unit testing only
                publish_event_to_topic(result_messages_list, failed_events_topic)
                continue

            # check if we have scraped info for this URL and CID in the MySQL already;
            # if yes - add the "PAGE_TEXT_COUNT" and "PAGE_TITLE" fields to the event
            # and send it to the "completed_activities" topic
            edc_rest_endpoint = EDC_REST_URL + config["edc_service_lookup_method_path"]
            enriched_message = lookup_scraping_info_from_db(message, edc_rest_endpoint)
            result_messages_list.append(enriched_message)  # this is for unit testing only
            page_text_count = enriched_message.get("PAGE_TEXT_COUNT", 0)
            page_title = enriched_message.get("PAGE_TITLE", None)
            # TODO figure out the logic - if one of them is empty - OK?
            if page_text_count != 0 and page_title is not None:
                completed_events_topic = config['completed_events_topic']
                print("Successfully looked up page info for URL={} and CID={}: "
                      "page_text_count={}, page_title={}; sending event to {} topic".format(
                    enriched_message.get("REF_PARAM"), enriched_message.get("CID"), page_text_count, page_title,
                    completed_events_topic
                ))
                publish_event_to_topic(result_messages_list, completed_events_topic)
            else:
                # if we do not have info for this URL scraped yet - send event for webscraping:
                events_to_webscrape_topic = config['events_to_webscrape_topic']
                print("No cached page info found for URL={} and CID={}: "
                      "sending event for webscraping to {} topic".format(
                    enriched_message.get("REF_PARAM"), enriched_message.get("CID"),
                    events_to_webscrape_topic
                ))
                publish_event_to_topic(result_messages_list, events_to_webscrape_topic)
            # if there were any errors during the lookup
            if enriched_message.get('ERRORS', []):
                print("There were errors processing event:{} - sending it to the {} topic: ".format(
                    json.dumps(enriched_message), failed_events_topic
                ))
                publish_event_to_topic(result_messages_list, failed_events_topic)

        return result_messages_list

    # if anything fails - send event to a failed events topic
    except Exception as e:
        err_str = str(e)
        if not data:
            messages = [
                {"CONFIG": CONFIG}
            ]
        if not messages:
            messages = [data]
        print("There was a fatal error processing event: {} - ERROR: - {}".format(json.dumps(messages), err_str))
        for message in messages:
            append_error(message, 'fatal_error', err_str)
        publish_event_to_topic(messages, failed_events_topic)


def lookup_scraping_info_from_db(event, edc_service_url):
    """
    function to call EDC microservice via a REST API to get cached page info for URL+CID, if available
    example REST API: https://primary-edc-inboundconverter.eng.techtarget.com/api/inbound-converter-urls?clientId.equals=100&clientUrl.equals=www.techtarget.com%2FhomepageURL_1

    """
    enriched_event = copy.deepcopy(event)
    url_to_scrape = event.get("REF_PARAM")
    cid = event.get("CID")
    query_params = {'clientId.equals': cid, 'clientUrl.equals': url_to_scrape, 'page': 0, 'size': 1,
                    'sort': 'dateCreated,desc'}
    try:
        response = requests.get(edc_service_url, params=query_params)
        json_response = response.json()
        if not json_response:
            print(
                "Completed call to {} for url {} OK - but no data was retrieved".format(edc_service_url, url_to_scrape))
        else:
            enriched_event['PAGE_TEXT_COUNT'] = json_response[0]["pageTextCount"]
            enriched_event['PAGE_TITLE'] = json_response[0]["pageTitle"]
            print("Completed call to {} for url {}: response: {}; enriched event: {}".format(edc_service_url,
                                                                                             url_to_scrape,
                                                                                             json_response,
                                                                                             enriched_event))
    except Exception as e:
        err_str = "client-activity-input: " + str(e)
        print("Error calling REST API {} for CID={} and url={}: error={}".format(edc_service_url, cid, url_to_scrape,
                                                                                 err_str))
        # append_error(enriched_event, "Failed call to REST API", err_str)

    return enriched_event


def publish_event_to_topic(event, topic):
    publisher = pubsub_v1.PublisherClient()
    data = json.dumps(event).encode("utf-8")
    # When you publish a message, the client returns a future.
    topic_path = publisher.topic_path(project, topic)
    publisher.publish(topic_path, data)


def process_webscrape_event(event, context):
    """
    Method to read event form PubSub topic, do webscraping for the REF_PARAM URL,
    and send the resulting enriched event off to another topic to be stored into BigQuery
    :param event: (dict) The dictionary with data specific to this type of
         event. The `data` field contains the PubsubMessage message. The
         `attributes` field will contain custom attributes if there are any.
    :return: (dict) post-processed data dictionary
    """
    data = {}
    try:
        config = get_web_scraper_config()
        failed_events_topic = config['failed_events_topic']
        completed_events_topic = config['completed_events_topic']
        if config['data_tag'] == 'attributes':
            data = json.loads(json.dumps(event['attributes']))
        elif config['data_tag'] == 'data':
            encoded_event = event['data']
            if encoded_event is not None:
                data = json.loads(base64.b64decode(encoded_event).decode('utf-8'))
                print("Processing input event: {}".format(json.dumps(data)))
            else:
                raise Exception('Wrong format of input event - no data key found; event: ' + event)
        else:
            raise Exception('No Data tag found in config')

        # TODO get rid of message lists - we are getting one event only, always
        input_messages_list = []
        if type(data) == list:
            input_messages_list = data
        else:
            input_messages_list.append(data)

        timeout = config['webscrape_timeout']
        result_messages_list = []
        for input_message in input_messages_list:
            if context:
                input_message['event_id'] = context.event_id
            result_message = page_content(input_message, timeout)
            if result_message.get('ERRORS', []):
                print("There were errors processing event:{} - sending it to the {} topic ".format(
                    json.dumps(result_message), failed_events_topic
                ))
                result_messages_list.append(result_message)  # this is for unit testing only
                publish_event_to_topic(result_messages_list, failed_events_topic)
            else:
                # remove leading and trailing white spaces and tabs from the page title
                page_title_stripped = result_message.get('PAGE_TITLE').strip()
                result_message['PAGE_TITLE'] = page_title_stripped
                print("Successfully webscraped event:{} - sending it to the {} topic ".format(
                    json.dumps(result_message), completed_events_topic
                ))
                result_messages_list.append(result_message)  # this is for unit testing only
                publish_event_to_topic(result_messages_list, completed_events_topic)
                # store results of the webscraping into the MySQL DB via EDC REST call
                edc_rest_endpoint = EDC_REST_URL + config["edc_service_store_method_path"]
                store_webscraping_results_to_db(result_message, edc_rest_endpoint)

        return result_messages_list

    except Exception as e:
        err_str = str(e)
        if not data:
            input_messages_list = [
                {"CONFIG": CONFIG}
            ]
        if not input_messages_list:
            input_messages_list = [data]
        print("There was a fatal error processing event: {} - ERROR: - {}".format(json.dumps(input_messages_list),
                                                                                  err_str))
        for message in input_messages_list:
            append_error(message, 'fatal_error', err_str)
        publish_event_to_topic(input_messages_list, failed_events_topic)


def store_webscraping_results_to_db(event, edc_service_url):
    """
    function to call EDC microservice via a REST API to store webscraped page info for URL+CID
    example REST API: ???
    """
    url = event.get("REF_PARAM")
    cid = event.get("CID")
    # TODO 2: what to do if this call fails? log only? store into failed_event??
    request_data = {
        "clientId": cid,
        "clientUrl": url,
        "pageContent": event.get('PAGE_CONTENT'),
        "pageTextCount": event.get('PAGE_TEXT_COUNT'),
        "pageTitle": event.get('PAGE_TITLE'),
        "pageType": event.get('PAGE_TYPE')
    }

    try:
        response = requests.post(edc_service_url, json=request_data)
        json_response = response.json()
        if not json_response:
            print("Completed call to {} OK; request_data={} - no response was received".format(
                edc_service_url, request_data))
        else:
            print("Completed call to {}: request_data: {}; response: {}".format(
                edc_service_url, request_data, json_response))
    except Exception as e:
        err_str = str(e)
        print("Error calling REST API {} to save request_data: {}: error={}".format(
            edc_service_url, request_data, err_str))
        # #TODO - decide if we should fail this event - if so:
        # append_error(event, "Failed call to REST API", err_str)

    return event


def apply_config_rules(messages, config, context=None):
    """
    Method to apply the main rules based on the configuration obtained from Firestore
    :param messages: (list) a list of the raw messages to apply rules to
    :param context: (dict) context provided from pubsub if applicable
    :return: (list, dict) a list of the post processed messages and the applicable config
    """

    out_messages = []
    for message in messages:
        if context:
            message['event_id'] = context.event_id
        data = process_rules(config=config, data=message)
        output_data = copy.deepcopy(data)
        # if the value of IBC_STORE_UNMATCHED is 0 - apply second set of rules
        if os.getenv('IBC_STORE_UNMATCHED') == "0":
            output_data = process_rules(config=config['load'], data=output_data)
        if output_data:
            out_messages.append(output_data)

    return out_messages


def create_process_blocks(config, output_list):
    """
    If post processing is involved, we can determine how/where to process the messages based on the configuration and
    a ruleset.
    :param config: (dict) the post-processing configuration
    :param output_list: (list) the list of messages to apply post-processing rules to
    :return: (dict) the configured topics and their associated messages
    """
    postprocesses = config.get("postprocess", [])

    events_per_topic_dict = {'ibc.failed-events': []}

    for postprocess in postprocesses:
        events_per_topic_dict[postprocess['topic']] = []

    for message in output_list:
        for postprocess in postprocesses:

            res = process_rules(config=postprocess, data=message)
            if res:
                if res.get('ERRORS', []):
                    events_per_topic_dict['ibc.failed-events'].append(res)
                else:
                    events_per_topic_dict[postprocess['topic']].append(res)

            else:
                # Message dropped and no other processing happens
                pass

    return events_per_topic_dict


def publish_list_to_pubsub(output_list, config):
    """
    A method to take the list of messages and check if there are any postprocessing steps that need to occur. If so,
    we process the messages to a post-processing topic. If not, we process to the next topic in the natural process.
    :param output_list: (list) A list of the messages that have been prepared for the next topic
    :param config: (dict) Main processing configuration
    :return: (dict) A dictionary of topics and their respective processing blocks
    """

    events_per_topic_dict = {}
    if output_list:
        publisher = pubsub_v1.PublisherClient()

        events_per_topic_dict = create_process_blocks(config, output_list)

        for topic in events_per_topic_dict:

            if events_per_topic_dict[topic]:
                print("About to send event to the PubSub topic={}; event: {}".format(
                    topic, json.dumps(events_per_topic_dict[topic])))
                data = json.dumps(events_per_topic_dict[topic]).encode("utf-8")
                # When you publish a message, the client returns a future.
                topic_path = publisher.topic_path(project, topic)
                publisher.publish(topic_path, data)

    return events_per_topic_dict


error_col = "ERRORS"


def append_error(process_data, error, description=""):
    error_obj = {"ERROR": error}
    if description:
        error_obj["DESCRIPTION"] = description.replace('\n', '')

    if process_data.get(error_col, []):
        process_data[error_col].append(error_obj)

    else:
        process_data[error_col] = [error_obj]


def handle_error(process_data, log_error, error):

    if log_error:
        append_error(process_data, error)
        return process_data

    else:
        return {}


def handle_message(process_data, err, log_error, err_message):
    if err:
        res = handle_error(process_data, log_error, err_message)

        return res

    else:
        return process_data


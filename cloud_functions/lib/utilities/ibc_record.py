"""
A Helper Class to deserialize an AVRO message
"""

import os

MAX_MESSAGES_TO_PROCESS = 4500
MAX_ITERATIONS = 20000

PARTITION = os.getenv('PARTITION')


class IbcRecord:
    """
    User record
    Args:
        name (str): User's name
        favorite_number (int): User's favorite number
        favorite_color (str): User's favorite color
    """

    def __init__(self, **kwargs):

        self.prop_dict = {key: kwargs[key] for key in kwargs}

    def get(self):
        """
        getter for nothing
        """

    def set(self):
        """
        setter for nothing
        """


def dict_to_ibc(obj, ctx):
    """
    Converts object literal(dict) to a User instance.
    Args:
        obj (dict): Object literal(dict)
        ctx (SerializationContext): Metadata pertaining to the serialization
            operation.
    """
    # pylint: disable=unused-argument
    if obj is None:
        return None
    return IbcRecord(**obj)


def avro_consumer(consumer):
    """
    Poll Avro records and return them deserialized
    :param consumer: () AVRO consumer to poll records
    :return: (list) A list of deserialized messages
    """
    # SIGINT can't be handled when polling, limit timeout to 1 second.
    running = True
    i = 0
    j = 0
    messages = []
    while running:
        msg = consumer.poll(1)
        # if msg is not None and msg.partition() == PARTITION:
        if msg is not None:
            ibc = msg.value()
            if ibc is not None:
                messages.append(msg)
                i += 1
                #consumer.commit(message=msg)  # need to verify this is the way to properly acknowledge message
            if i > MAX_MESSAGES_TO_PROCESS:
                running = False
        j += 1
        if j > MAX_ITERATIONS:  # stop gap for message loops so it doesnt infinitely loop
            running = False
            print("Max iterations hit")

    return messages
    # Process message
    # commit at the end of processing

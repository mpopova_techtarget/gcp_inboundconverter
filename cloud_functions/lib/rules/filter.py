"""
Module to define releset for value filtering
"""
from lib.rules.defaults import get_default_list


def filter_key_default(process_data, config):
    """
    Function to return empty object if a particular key is in the current defaults
    :param process_data: (dict) Input processing object
    :param config: (dict) Rule configuration
    :return: (dict) Original input dictionary if value is not None or {}
    """
    return {} if process_data.get(config['column']) in get_default_list() else process_data


def filter_key_none(process_data, config):
    """
    Function to return empty object if a particular key is None
    :param process_data: (dict) Input processing object
    :param config: (dict) Rule configuration
    :return: (dict) Original input dictionary if value is not None or {}
    """
    return {} if process_data.get(config['column']) is None else process_data


def filter_key_true(process_data, config):
    """
    Function to return empty object if a particular key is True
    :param process_data: (dict) Input processing object
    :param config: (dict) Rule configuration
    :return: (dict) Original input dictionary if value is not None or {}
    """
    return {} if process_data.get(config['column']) else process_data


def filter_key_false(process_data, config):
    """
    Function to return empty object if a particular key is False
    :param process_data: (dict) Input processing object
    :param config: (dict) Rule configuration
    :return: (dict) Original input dictionary if value is not None or {}
    """
    return process_data if process_data.get(config['column']) else {}


def keep_record_if(process_data, config):
    """
    Function to return object if value is resolves conditional
    :param process_data: (dict) Input processing object
    :param config: (dict) Rule configuration
    :return: (dict) Original input dictionary if value is not None or {}
    """
    if config['operator'] == "==":
        return process_data if process_data.get(config['column']) == config['value'] else {}


def get_rules():
    """
    Return ruleset
    :return: (dict) Dictionary of methods to be used within this ruleset
    """
    return {
        "filter_key_none": filter_key_none,
        "filter_key_default": filter_key_default,
        "filter_key_false": filter_key_false,
        "filter_key_true": filter_key_true,
        "keep_record_if": keep_record_if
    }

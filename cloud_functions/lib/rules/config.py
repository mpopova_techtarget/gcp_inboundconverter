"""
Helper module to get configs from source in lieu of having to reach out to FS each time

"""

def get_inbound_converter_config():
    """

    """
    return {
      "data_tag": "attributes",
      "edc_service_lookup_method_path": "/api/inbound-converter-urls",
      "completed_events_topic": "ibc.completed-activities",
      "failed_events_topic": "ibc.failed-events",
      "events_to_webscrape_topic": "ibc.events-to-webscrape",
      "rules": [
        {"rule": "filter_blacklist_client_ids", "rule_set": "inbound_converter"},
        {"rule": "filter_blacklist_urls", "rule_set": "inbound_converter"},
        {"rule": "is_eu_allowed", "rule_set": "inbound_converter", "var": "is_eu_allowed", "is_eu_allowed": [1, 2, 3, 4, 5]},
        {"rule": "get_gdpr_region", "rule_set": "inbound_converter", "var": "REGION_CD", "collection": "maxmind"},
        {"rule": "is_gdpr_allowed", "rule_set": "inbound_converter", "var": "EU_ALLOWED"},
        {"rule": "get_external_id", "rule_set": "inbound_converter", "collection": "kickfire", "var": "DOMAIN"},
        {"rule": "decode_url", "rule_set": "inbound_converter", "var": "REF_PARAM", "input_col": "REF_PARAM"},
        {"rule": "classify_url", "rule_set": "classification", "var": "PAGE_TYPE", "fields": ["REF_PARAM"]},
        {"rule": "add_audit_columns", "rule_set": "inbound_converter"},
        {"rule": "add_partition_columns", "rule_set": "inbound_converter"}
      ],
        "postprocess": [
            {"topic": "ibc.events-to-webscrape", "rules": [{"rule": "filter_key_default", "column": "DOMAIN", "rule_set": "filter_inbound_converter"}]}
        ],
        "load": {
            "rules": [
                {"rule": "filter_key_default", "column": "DOMAIN", "rule_set": "filter_inbound_converter"},
                {"rule": "filter_key_true", "column": "IS_ISP", "rule_set": "filter_inbound_converter"}
            ]
        }
    }


def get_write_inbound_converter_config():
    return {
        "load": {
            "tables": [
                {
                    "table_name": "{}.edc_inboundconverter.inbound_converter",
                    "columns": [
                        "LOGSTASH_ID", "ACTIVITY_TYPE_ID", "VERSION", "CID", "REF_PARAM", "REFERER", "USER_AGENT",
                        "REGION_CD", "ACTIVITY_DATE", "EU_ALLOWED", "DOMAIN", "ACTIVITY_MILLIS", "IS_ISP",
                        "DATE_CREATED", "DATE_MODIFIED", "PAGE_TYPE", "PAGE_TITLE", "PAGE_TEXT_COUNT"
                    ],
                    "enabled": True
                },
                {
                    "table_name": "{}.edc_inboundconverter.inbound_converter_ip_lookup",
                    "columns": ["LOGSTASH_ID", "ACTIVITY_IP", "ACTIVITY_DATE"],
                    "enabled": True,
                    "rules": [
                        {"rule": "filter_key_false", "column": "EU_ALLOWED", "rule_set": "filter_inbound_converter"}]
                }
            ]
        }
    }


def get_write_inbound_converter_error_config():
    """

    """
    return {
        "load": {
            "tables": [
                {
                    "table_name": "{}.edc_inboundconverter.inbound_converter_failures",
                    "columns": [
                        "LOGSTASH_ID", "ACTIVITY_TYPE_ID", "VERSION", "CID", "REF_PARAM", "REFERER", "USER_AGENT",
                        "REGION_CD", "ACTIVITY_DATE", "EU_ALLOWED", "DOMAIN", "ACTIVITY_MILLIS", "IS_ISP",
                        "DATE_CREATED", "DATE_MODIFIED", "PAGE_TYPE", "PAGE_TITLE", "PAGE_TEXT_COUNT", "ERRORS"
                    ],
                    "enabled": True
                }
            ]
        }
    }


def get_web_scraper_config():
    """

    """
    return {
        "data_tag": "data",
        "edc_service_store_method_path": "/api/persist/url",
        "completed_events_topic": "ibc.completed-activities",
        "failed_events_topic": "ibc.failed-events",
        "webscrape_timeout": 5
     }


def get_config(config):

    confs = {
        "InboundConverter": get_inbound_converter_config(),
        "WebScraper": get_web_scraper_config(),
        "WriteInboundConverter": get_write_inbound_converter_config(),
        "WriteInboundConverterErrors": get_write_inbound_converter_error_config()
    }

    return confs[config]

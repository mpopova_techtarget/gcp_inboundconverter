"""
Helper module to return the default values in various types
"""


def get_default_list():
    """
    Method to return all default values as a list
    to reduce the number of columns in any input to fit the format of the table
    :return: (list) default values
    """
    return ['--', -1, None]


def get_default_dict():
    """
    Method to return all default values as a dictionary
    to reduce the number of columns in any input to fit the format of the table
    :return: (dict) method to return the defaults values as a dictionary. The keys represent the type.
    """
    return {
        "STRING": '--',
        "DATE": None,
        "INTEGER": -1,
        "PAGE_TITLE": "IBC_NO_TITLE"
    }

"""
Module to define releset for value filtering
"""
from lib.rules.defaults import get_default_list
from lib.utilities.error_handler import handle_message


def filter_key_default(process_data, config):
    """
    Function to return empty object if a particular key is in the current defaults
    :param process_data: (dict) Input processing object
    :param config: (dict) Rule configuration
    :return: (dict) Original input dictionary if value is not None or {}
    """
    err = True if process_data.get(config['column']) in get_default_list() else False
    err_message = "Column {} is a default value or does not exist".format(config['column'])
    log_error = config.get("log_error", False)

    return handle_message(process_data, err, log_error, err_message)


def filter_key_none(process_data, config):
    """
    Function to return empty object if a particular key is None
    :param process_data: (dict) Input processing object
    :param config: (dict) Rule configuration
    :return: (dict) Original input dictionary if value is not None or {}
    """

    err = True if process_data.get(config['column']) is None else False
    err_message = "Column {} is None".format(config['column'])
    log_error = config.get("log_error", False)

    return handle_message(process_data, err, log_error, err_message)


def filter_key_true(process_data, config):
    """
    Function to return empty object if a particular key is True
    :param process_data: (dict) Input processing object
    :param config: (dict) Rule configuration
    :return: (dict) Original input dictionary if value is not None or {}
    """

    err = True if process_data.get(config['column']) else False
    err_message = "Column {} is True".format(config['column'])
    log_error = config.get("log_error", False)

    return handle_message(process_data, err, log_error, err_message)


def filter_key_false(process_data, config):
    """
    Function to return empty object if a particular key is False
    :param process_data: (dict) Input processing object
    :param config: (dict) Rule configuration
    :return: (dict) Original input dictionary if value is not None or {}
    """

    err = False if process_data.get(config['column']) else True
    err_message = "Column {} is False, None or does not exist".format(config['column'])
    log_error = config.get("log_error", False)

    return handle_message(process_data, err, log_error, err_message)


def keep_record_if(process_data, config):
    """
    Function to return object if value is resolves conditional
    :param process_data: (dict) Input processing object
    :param config: (dict) Rule configuration
    :return: (dict) Original input dictionary if value is not None or {}
    """
    if config['operator'] == "==":
        err = False if process_data.get(config['column']) == config['value'] else True
        err_message = "Column {} is equals {}".format(config['column'], config['value'])
        log_error = config.get("log_error", False)

        return handle_message(process_data, err, log_error, err_message)


def get_rules():
    """
    Return ruleset
    :return: (dict) Dictionary of methods to be used within this ruleset
    """
    return {
        "filter_key_none": filter_key_none,
        "filter_key_default": filter_key_default,
        "filter_key_false": filter_key_false,
        "filter_key_true": filter_key_true,
        "keep_record_if": keep_record_if
    }

"""
A ruleset to define those rules defined as General
"""
import copy
import json

from lib.rules.inbound_converter import get_rules as get_ibc_rules
from lib.rules.filter import get_rules as get_filter_rules
from lib.rules.page_type import get_rules as get_page_type_rules
from lib.rules.page_content import get_rules as get_page_content_rules


def get_rules(rule_set):
    """
    Retrieve ruleset based on input
    :param rule_set: (str) rule set to retrieve
    :return: (dict) rule definitions
    """
    rules = {
        "inbound_converter": get_ibc_rules(),
        "filter_inbound_converter": get_filter_rules(),
        "classification": get_page_type_rules(),
        "page_content": get_page_content_rules()
    }

    return rules[rule_set]


def process_rules(config, data):
    """
    General configuration for processing ibc rules
    :param config: (dict) dictionary containing processing parameters
    :param data: (dict) input dictionary to be used for processing
    :return: (dict) resultant object with rules applied
    """

    process_data = copy.deepcopy(data)
    rules = copy.deepcopy(config.get('rules', []))
    for rule_config in rules:
        rule_set_name = rule_config['rule_set']
        rule_name = rule_config['rule']
        rule_set = get_rules(rule_set_name)
        rule_method = rule_set[rule_name]
        process_data = rule_method(process_data=process_data, config=rule_config)
        if process_data == {}:
            break

    return process_data


def filter_columns(data, columns):
    """
    Rules are based on the original keys based in the dictionary. We dont want to load all those keys so we need a way
    to reduce the number of columns in any input to fit the format of the table
    :param data: (dict) input dictionary to be used for processing
    :param columns: (list) List of all columns to keep
    :return: (dict) resultant object with only the columns to be loaded into BQ
    """

    filter_data = copy.deepcopy(data)
    if columns is not None and data != {}:
        filter_data = {}
        for key in columns:
            if key in data:
                filter_data[key] = data[key]

    return filter_data

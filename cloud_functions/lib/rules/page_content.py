"""

"""

import urllib
from urllib.error import HTTPError, URLError
import charade
import socket

from lxml.html import fromstring
from boilerpy3 import extractors

from lib.utilities.error_handler import append_error

headers = {'User-Agent': 'Mozilla/5.0'}


def get_html(url, timeout=None):
    """ Adopted from boilerpipe3 __init__.py.
    Gets html for a url in exact same way boilerpipe would.
    Only difference is can control timeout which was not possible via boilerpipe.
    """
    html, error, status = None, None, None

    try:
        request = urllib.request.Request(url, headers=headers)
        connection = urllib.request.urlopen(request, timeout=timeout)
        # Extract and encode html.
        data = connection.read()
        encoding = connection.headers['content-type'].lower().split('charset=')[-1]
        if encoding.lower() == 'text/html':
            encoding = charade.detect(data)['encoding']
        if encoding is None:
            encoding = 'utf-8'
        html = str(data, encoding)
        status = 'OK'
    except URLError as urlerror:
        if type(urlerror.reason) == socket.timeout:
            status = 'Timeout'
            error = str(urlerror)
        else:
            status = 'Other'
            error = str(urlerror)

    except HTTPError:
        status = 'HTTPError'
        error = str(HTTPError)

    except ValueError:
        status = 'ValueError'
        error = str(ValueError)

    except Exception as myex:
        status = 'Other'
        error = str(myex)

    return status, html, error


def scrape_html(html, extract_type='KeepEverythingExtractor'):
    """ Given html extract text and title.
    Note: moved input from url to html in order to control timeout.

    Example
    -------
    url = 'http://hs-sites.origin.hubspot.net'
    status, html = get_html(url, timeout=1)
    title, text = extract_from_html(html)
    """
    title, text, error = None, None, None

    try:

        title = ''
        extractor = extractors.KeepEverythingExtractor()

        text = extractor.get_content(html)

        tree = fromstring(html)
        title = tree.findtext('.//title')

    except Exception as my_exception:
        error = str(my_exception)

    return title, text, error


def page_content(process_data, timeout):
    """

    """
    url = process_data["REF_PARAM"]
    status, html, error = get_html(url, timeout=timeout)
    if status == "OK":
        title, text, error = scrape_html(html)

        if not title:
            append_error(process_data, "no_page_title", error)
        else:
            process_data['PAGE_TITLE'] = title

        if not text:
            append_error(process_data, "no_page_text", error)
        else:
            text_list = text.split('\n')
            process_data['PAGE_TEXT_COUNT'] = len(text_list)
            # TODO figure out how to safely store page content into MySQL
            process_data['PAGE_CONTENT'] = "TBD"

    else:
        append_error(process_data, 'failure_scrape', error)

    return process_data


def get_rules():
    """
    Return ruleset
    :return: (dict) Dictionary of methods to be used within this ruleset
    """
    return {
        "page_content": page_content
    }

"""
Ruleset specific to Inbound Converter transformations
"""
import json
import os
from urllib.parse import unquote
from datetime import datetime

from netaddr import IPNetwork
from google.cloud import firestore

from lib.rules.defaults import get_default_dict


def filter_blacklist_urls(process_data, config):
    field_to_check = "REF_PARAM"
    url_value = unquote(process_data[field_to_check])
    # load blacklisted url list
    my_path = os.path.abspath(os.path.dirname(__file__))
    with open(os.path.join(my_path, "blacklist_urls.txt")) as f:
        bl_urls_list = f.read().splitlines(keepends=False)

    if url_value in bl_urls_list:
        # it matched
        # TODO add more complex RegEx matching later
        print('dropping event due to url_value={} matching blacklisted URLs; event={}'.format(
            url_value, json.dumps(process_data)))
        process_data = {}
    else:
        # TMP logging - to troubleshoot not blocked events with blacklisted URLs
        print('NOT dropping event: url_value={} did not match any blacklisted URLs; event={}'.format(
            url_value, json.dumps(process_data)))

    return process_data


def filter_blacklist_client_ids(process_data, config):
    field_to_check = "CID"
    cid_value = process_data[field_to_check]
    # if this field is empty - the event is not valid, drop it
    # TODO - move to a dedicated validation - like the first check before any rules are applied
    #if not cid_value:
       # print('\n dropping event due to empty CID field: ' + json(process_data))
       # return {}

    # load blacklisted client ids list
    my_path = os.path.abspath(os.path.dirname(__file__))
    with open(os.path.join(my_path, "blacklist_client_ids.txt")) as f:
        bl_cids_list = f.read().splitlines(keepends=False)

    if cid_value in bl_cids_list:
        # it matched - drop this event
        print('\n dropping event due to matching blacklisted client IDs: ' + json.dumps(process_data))
        process_data = {}

    return process_data


def decode_url(process_data, config):
    """
    Function to decode the fully qualified url path
    :param process_data: (dict) Input processing object
    :param config: (dict) Rule configuration
    :return: (dict) Updated data
    """

    process_data[config['var']] = unquote(process_data[config['input_col']])
    return process_data


def is_eu_allowed(process_data, config):
    """
    Function to determine add variable to determine if region is in GDPR whitelist
    :param process_data: (dict) Input processing object
    :param config: (dict) Rule configuration
    :return: (dict) Updated data
    """
    process_data[config['var']] = process_data['CID'] in config['is_eu_allowed']
    return process_data


def get_value_from_ip(process_data, config):
    """

    :param process_data: (dict) Input processing object
    :param config: (dict) Rule configuration
    :return: (dict) Updated data
    """
    network = IPNetwork(process_data[config['column']])
    int_ip = int(network[0])

    fs_client = firestore.Client()
    site = fs_client.collection(config['collection'])

    # we can do a compound query on different fields in FS using inequalities so we are using a little logic to return
    # the closest value to the lookup. Can think about extending this to > 1 return if desired
    query = site.where('start_ip_int', "<=", int_ip).order_by('start_ip_int',
                                                              direction=firestore.Query.DESCENDING).limit(1)

    res = query.stream()

    # stream returns a generator so we have to loop although there will only be 1 result
    for doc in res:
        res_dict = doc.to_dict()
        if res_dict['start_ip_int'] <= int_ip <= res_dict['end_ip_int']:
            process_data[config['var']] = res_dict[config['document_var']]

        else:
            process_data[config['var']] = get_default_dict()['STRING']

    return process_data


def get_gdpr_region(process_data, config):
    """
    Function to update the region in which the incoming IP came from
    :param process_data: (dict) Input processing object
    :param config: (dict) Rule configuration
    :return: (dict) Updated data
    """
    network = IPNetwork(process_data['ACTIVITY_IP'])
    int_ip = int(network[0])

    fs_client = firestore.Client()
    site = fs_client.collection(config['collection'])

    # we can do a compound query on different fields in FS using inequalities so we are using a little logic to return
    # the closest value to the lookup. Can think about extending this to > 1 return if desired
    query = site.where('start_ip_int', "<=", int_ip).order_by('start_ip_int',
                                                              direction=firestore.Query.DESCENDING).limit(1)
    res = query.stream()

    # stream returns a generator so we have to loop although there will only be 1 result
    for doc in res:
        res_dict = doc.to_dict()
        if res_dict['start_ip_int'] <= int_ip <= res_dict['end_ip_int']:
            process_data[config['var']] = res_dict['country_iso_code']

        else:
            process_data[config['var']] = get_default_dict()['STRING']

    return process_data


def is_gdpr_allowed(process_data, config):
    """
    Function to determine id value is able to be looked up based on GDPR exclusions
    :param process_data: (dict) Input processing object
    :param config: (dict) Rule configuration
    :return: (dict) Updated data
    """
    exclude_regions = ["SI", "TR", "BY", "LT", "CH", "ES", "GE", "CZ", "AD", "AL", "IS", "IT", "FR", "BE", "LU", "AM",
                       "EE", "VA", "GR", "MD", "FI", "HR", "CY", "DK", "SE", "PT", "IE", "HU", "LV", "MC", "ME", "GB",
                       "DE", "AT", "BA", "RO", "BG", "LI", "MT", "MK", "PL", "UA", "XK", "KZ", "SK", "NL", "AZ"]
    process_data[config['var']] = process_data['is_eu_allowed'] or process_data['REGION_CD'] not in exclude_regions

    return process_data


def add_partition_columns(process_data, config):
    """
    Function to update the columns to fit the partitioning structure as well as adjust for UTC
    :param process_data: (dict) Input processing object
    :param config: (dict) Rule configuration
    :return: (dict) Updated data
    """
    # pylint: disable=unused-argument
    activity_date = datetime.utcfromtimestamp(int(process_data['ACTIVITY_DATE']) / 1000.0)
    process_data['ACTIVITY_DATE'] = activity_date.strftime('%Y-%m-%d')
    process_data['ACTIVITY_MILLIS'] = int(activity_date.timestamp() * 1000)

    return process_data


def add_audit_columns(process_data, config):
    """
    Function to add audit columns to result set
    :param process_data: (dict) Input processing object
    :param config: (dict) Rule configuration
    :return: (dict) Updated data
    """
    # pylint: disable=unused-argument
    now = datetime.utcnow().strftime('%Y-%m-%d %X')
    process_data['DATE_CREATED'] = now
    process_data['DATE_MODIFIED'] = now

    return process_data


def get_external_id(process_data, config):
    """
    Fucntion to lookup source domain based on IP input
    :param process_data: (dict) Input processing object
    :param config: (dict) Rule configuration
    :return: (dict) Updated data
    """
    network = IPNetwork(process_data['ACTIVITY_IP'])
    int_ip = int(network[0])

    fs_client = firestore.Client()
    site = fs_client.collection(config['collection'])

    # we can do a compound query on different fields in FS using inequalities so we are using a little logic to return
    # the closest value to the lookup. Can think about extending this to > 1 return if desired
    query = site.where('start_ip_int', "<=", int_ip).order_by('start_ip_int',
                                                              direction=firestore.Query.DESCENDING).limit(1)

    res = query.stream()

    # stream returns a generator so we have to loop although there will only be 1 result
    for doc in res:
        res_dict = doc.to_dict()
        if res_dict['start_ip_int'] <= int_ip <= res_dict['end_ip_int']:
            process_data[config['var']] = res_dict['Website']

        else:
            process_data[config['var']] = get_default_dict()['STRING']

        process_data['IS_ISP'] = res_dict.get('is_isp', False)

    return process_data


def get_rules():
    """
    Return ruleset
    :return: (dict) Dictionary of methods to be used within this ruleset
    """
    return {
        "filter_blacklist_client_ids": filter_blacklist_client_ids,
        "filter_blacklist_urls": filter_blacklist_urls,
        "is_eu_allowed": is_eu_allowed,
        "get_gdpr_region": get_gdpr_region,
        "is_gdpr_allowed": is_gdpr_allowed,
        "get_external_id": get_external_id,
        "get_value_from_ip": get_value_from_ip,
        "decode_url": decode_url,
        "add_audit_columns": add_audit_columns,
        "add_partition_columns": add_partition_columns
    }

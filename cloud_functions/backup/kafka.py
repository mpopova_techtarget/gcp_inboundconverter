from confluent_kafka import DeserializingConsumer
from confluent_kafka.schema_registry import SchemaRegistryClient
from confluent_kafka.schema_registry.avro import AvroDeserializer
from confluent_kafka.serialization import StringDeserializer

import json
import os

from lib.utilities.ibc_record import dict_to_ibc, avro_consumer
from main import apply_config_rules, publish_list_to_pubsub

ENV = os.getenv('ENV')
URL_SUFFIX = "{}.techtarget.com".format(ENV)
PARTITION = os.getenv('PARTITION')


def create_consumer():
    """
    Method to create a consumer for the Kafka data processing
    :return: (DeserializingConsumer)
    """

    schema = json.dumps({
        "type": "record",
        "name": "KsqlDataSourceSchema",
        "namespace": "io.confluent.ksql.avro_schemas",
        "fields": [
          {"name": "USER_AGENT", "type": ["null", "string"], "default":None},
          {"name": "LOGSTASH_ID", "type": ["null", "string"], "default":None},
          {"name": "ACTIVITY_IP", "type": ["null", "string"], "default":None},
          {"name": "REFERER", "type": ["null", "string"], "default":None},
          {"name": "CID", "type": ["null", "string"], "default":None},
          {"name": "VERSION", "type": ["null", "string"], "default":None},
          {"name": "AUTH", "type": ["null", "string"], "default":None},
          {"name": "LAST_VISITED", "type": ["null", "string"], "default":None},
          {"name": "REF_PARAM", "type": ["null", "string"], "default":None},
          {"name": "ACTIVITY_TYPE_ID", "type": ["null", "int"], "default":None},
          {"name": "ACTIVITY_DATE", "type": ["null", "long"], "default":None}
        ]
      })
    sr_conf = {"url": "http://cschema1-1.{}:8081".format(URL_SUFFIX)}
    schema_registry_client = SchemaRegistryClient(sr_conf)

    print("got config")
    avro_deserializer = AvroDeserializer(schema, schema_registry_client, dict_to_ibc)
    string_deserializer = StringDeserializer('utf_8')
    bootstrap_servers = 'ckafka1-1.{URL_SUFFIX}:9092,ckafka1-2.{URL_SUFFIX}:9092,ckafka1-3.{URL_SUFFIX}:9092'\
        .format(URL_SUFFIX=URL_SUFFIX)

    consumer_conf = {'bootstrap.servers': bootstrap_servers,
                     'key.deserializer': string_deserializer,
                     'value.deserializer': avro_deserializer,
                     'group.id': 'groupid_{}'.format(PARTITION),
                     'auto.offset.reset': "earliest",
                     "enable.auto.commit": False}

    consumer = DeserializingConsumer(consumer_conf)
    consumer.subscribe(['db.ap.client.traffic'])

    return consumer


def process_kafka_avro(request):
    """
    A method to poll a kafka stream and return the serialized messages
    :param request: default param to receive from Scheduler for a http request
    :return: (list) list of the post processed messages
    """
    # pylint: disable=unused-argument
    consumer = create_consumer()
    recs = avro_consumer(consumer)

    insert_messages, config = apply_config_rules(messages=recs)
    if insert_messages:
        msg_data = []
        for message in insert_messages:
            msg_data.append(message.prop_dict)
            consumer.commit(message=message)
        publish_list_to_pubsub(output_list=msg_data, config=config)

    return insert_messages

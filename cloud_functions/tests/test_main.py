import base64
import copy
import json

import pytest
import main
from unittest.mock import patch

from tests.conftest import read_resource_file
from tests.lib.gcp import pubsub_v1, bigquery


@pytest.mark.parametrize(
    "directory, event_filename, expected_filename, event_data_tag", [
        ("process_event", "1_input_good.json", "1_expected_good.json", "attributes"),
    ]
)
def test_process_event(
        directory, create_event_from_json, event_data_tag,
        event_filename, generate_context,
        generate_expected, expected_filename, monkeypatch
):
    with patch('main.pubsub_v1.PublisherClient') as mock_pubsub_v1:
        # we have to patch the return of apply_config_rules as it generates a real time datetime and we dont
        # have a way of mocking that part that deep. We have to rely on the results of the individual rule
        # testing to verify this part, as well as a test of apply_config_rules itself to verify
        with patch("main.apply_config_rules") as config_rules:
            expected_result_event = generate_expected
            expected_result_event_list = []
            expected_result_event_list.append(expected_result_event)
            config_rules.return_value = expected_result_event_list
            monkeypatch.setenv("IBC_STORE_UNMATCHED", "1")
            mock_pubsub_v1.return_value = pubsub_v1.PublisherClient()
            result_event_list = main.process_event(create_event_from_json, generate_context)
            assert result_event_list == expected_result_event_list

"""
#TODO fix this
@pytest.mark.parametrize(
    "directory, event_filename, expected_filename" [
        ("test_rest_lookup", "1_input_good.json", "1_expected_good.json")
        ,("test_rest_lookup", "2_input_no_match.json", "2_expected_no_match.json")
    ]
)
def test_lookup_scraping_info_from_db_good_match(
        directory, event_filename, expected_filename
):
"""


@pytest.mark.skip(reason="results from the RESt endpoint have to be mocked - otherwise you depend on existance of data in a test Db ")
# TODO implement with mocking of the REST call results
def test_lookup_scraping_info_from_db_good_match():
    input_event = json.loads(read_resource_file("test_rest_lookup", "1_input_good.json"))
    expected_event = json.loads(read_resource_file("test_rest_lookup", "1_expected_good.json"))
    edc_service_url = "https://primary-edc-inboundconverter.eng.techtarget.com/api/inbound-converter-urls"
    result_event = main.lookup_scraping_info_from_db(input_event, edc_service_url)
    assert result_event == expected_event


# TODO implement with mocking of the REST call results
def test_lookup_scraping_info_from_db_no_match():
    input_event = json.loads(read_resource_file("test_rest_lookup", "2_input_no_match.json"))
    expected_event = json.loads(read_resource_file("test_rest_lookup", "2_expected_no_match.json"))
    edc_service_url = "https://primary-edc-inboundconverter.eng.techtarget.com/api/inbound-converter-urls"
    result_event = main.lookup_scraping_info_from_db(input_event, edc_service_url)
    assert result_event == expected_event


# TODO implement with mocking of the REST call results
def test_store_webscraping_results_to_db():
    input_event = json.loads(read_resource_file("test_rest_store", "1_input_good.json"))
    expected_event = json.loads(read_resource_file("test_rest_store", "1_expected_good.json"))
    edc_service_url = "https://primary-edc-inboundconverter.eng.techtarget.com/api/inbound-converter-urls"
    result_event = main.store_webscraping_results_to_db(input_event, edc_service_url)
    # TODO add real verification of the results of the REST call
    assert result_event == expected_event


@pytest.mark.parametrize(
    "directory, event_filename, expected_filename, event_data_tag", [
        ("process_webscrape_event", "1_input_good.json", "1_expected_good.json", "data"),
        #("process_webscrape_event", "3_input_title_with_spaces.json", "1_expected_good.json", "data"),
        #("process_webscrape_event", "2_input_testurl.json", "1_expected_good.json", "data"),
        #("process_webscrape_event", "2_input_missing_required_fields.json", "2_expected_missing_required_fields.json", "data"),
    ]
)
@pytest.mark.skip(reason="page_text_count for the same URL is changing often - so have to adjust the expected value; "
                         "otherwise, this test works")
# TODO the test with 2_input_missing_required_fields.json should fail - as the same input event failes webscraping in GCP
# however, it does not fail with the same error in the unit test - have to investigate
def test_process_webscrape_event(
        directory, create_event_from_json, event_data_tag,
        event_filename, generate_expected, generate_context, expected_filename
):
    with patch('main.pubsub_v1.PublisherClient') as mock_pubsub_v1:
        mock_pubsub_v1.return_value = pubsub_v1.PublisherClient()
        result_event = main.process_webscrape_event(create_event_from_json, generate_context)
        assert result_event == generate_expected


@pytest.mark.parametrize(
    "directory, event_filename, expected_filename, config, event_data_tag", [
        ("handle_failed_event", "1_event.json", "1_expected.json", "WriteInboundConverterErrors", "data"),
    ]
)
def test_handle_failed_event(directory, create_event_from_json, event_filename,
                             generate_context, generate_expected, expected_filename,
                             config, event_data_tag):
    with patch('main.bigquery.Client') as mock_bigquery:
        with patch.object(mock_bigquery, 'insert_rows_json', None):
            with patch("main.CONFIG", config):
                mock_bigquery.return_value = bigquery.Client()
                main.handle_failed_event(create_event_from_json, generate_context)


@pytest.mark.parametrize(
    'directory, input_filename, config_name, expected_filename', [
        ('apply_config_rules', '1_process_data.json', 'InboundConverter', '1_expected.json'),
        ('apply_config_rules', '2_process_data.json', 'WebScraper', '2_expected.json'),
    ]
)
@pytest.mark.skip(reason="Incomplete Test")
def test_apply_config_rules(
        input_filename, generate_input,
        config_name, get_config,
        expected_filename, generate_expected,
        directory
):
    assert main.apply_config_rules(generate_input, get_config) == generate_expected


@pytest.mark.parametrize(
    'directory, input_filename, config_name, expected_filename', [
        ('create_process_blocks', '1_process_data.json', 'InboundConverter', '1_expected.json'),
        ('create_process_blocks', '2_process_data.json', 'WebScraper', '2_expected.json'),
    ]
)
@pytest.mark.skip(reason="Incomplete Test")
def test_create_process_blocks(
        input_filename, generate_input,
        config_name, get_config,
        expected_filename, generate_expected,
        directory
):
    assert main.create_process_blocks(get_config, generate_input) == generate_expected

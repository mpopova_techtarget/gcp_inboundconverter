import pytest
from lib.rules.page_type import *


@pytest.mark.parametrize(
    'directory, input_filename, config_filename, expected_filename', [
        ('classify_url', '1_process_data.json', 'general_rule.json', '1_expected.json'),
        ('classify_url', '2_process_data.json', 'general_rule.json', '2_expected.json'),
        ('classify_url', '3_process_data.json', 'general_rule.json', '3_expected.json'),
        ('classify_url', '4_process_data.json', 'general_rule.json', '4_expected.json')
    ]
)
def test_classify_url(
        generate_input, input_filename,
        generate_config, config_filename,
        generate_expected, expected_filename,
        directory
):
    assert classify_url(process_data=generate_input, config=generate_config) == generate_expected

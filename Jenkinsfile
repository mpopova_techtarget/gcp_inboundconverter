#!groovy

pipeline {
    agent {
        label 'jenkinsci-deploy'
    }

    environment {
        pythonVenv = "python3 -m virtualenv --python python3 cloud_functions/venv/build"
        activateVenv = ". ${WORKSPACE}/venv/build/bin/activate"
        requirements = "venv/build/bin/pip install --upgrade pip && cloud_functions/venv/build/bin/pip install -r cloud_functions/requirements.txt"
        pylint = "pylint *"
        pytest = "pytest "
        //removeVenv = "rm -rf venv/build"

        region = "us-east4"
        project = "tt-pd-${ENVIRONMENT}"
        runtime = "python37"
        service_account = "tt-ibc@${project}.iam.gserviceaccount.com"
        runner_account = "tt-ci-cd-exec@${project}.iam.gserviceaccount.com"
        source = "./cloud_functions"
        memory = "128MB"
        labels = "tt-component=cloud-function,tt-environment=${ENVIRONMENT},tt-product=inbound-converter,tt-region=us-east4,tt-service=cloud-functions,tt-version=na,tt-zone=multi"

    }
    stages {
        stage ('Unit Testing') {
            steps {
                catchError(buildResult: 'SUCCESS', stageResult: 'FAILURE') {
                    echo "${env.project}"
                    sh("${env.pythonVenv} && ${env.activateVenv} && ${env.requirements} && ${env.pytest}")
                    sh "exit 1"
                }    
            }
        }

        stage ('Deploy') {
            stages {
                stage ('Activate SA') {
                    steps {
                        sh("gcloud config set account ${env.runner_account}")
                        //sh('venv/build/bin/python edc_activity/src/main/cloud_function/inbound_converter/cloud_functions/load_config.py')
                    }
                }
                stage ('Deploy CF') {
                    parallel {
                        stage ("client_activity_input") {
                            steps {
                                sh("gcloud functions deploy client_activity_input --project ${env.project} --region=${env.region} --runtime ${env.runtime} --service-account ${env.service_account} --source=${env.source} --trigger-topic ibc.client-activity-input --entry-point process_event --stage-bucket ibc-client-activity-input-${ENVIRONMENT} --memory=${env.memory} --set-env-vars PROJECT=${env.project},IBC_STORE_UNMATCHED=${IBC_STORE_UNMATCHED},CONFIG=InboundConverter --update-labels=${env.labels}")
                            }
                        }
                        stage ("write_client_activity") {
                            steps {
                                sh("gcloud functions deploy write_client_activity --project ${env.project} --region=${env.region} --runtime ${env.runtime} --service-account ${env.service_account} --source=${env.source} --trigger-topic ibc.completed-activities --entry-point write_to_bq --stage-bucket ibc-client-activity-input-${ENVIRONMENT} --memory=${env.memory} --set-env-vars PROJECT=${env.project},CONFIG=WriteInboundConverter --update-labels=${env.labels}")
                            }
                        }
                        stage ("web_scraper") {
                            steps {
                                sh("gcloud functions deploy web_scraper --project ${env.project} --region=${env.region} --runtime ${env.runtime} --service-account ${env.service_account} --source=${env.source} --trigger-topic ibc.events-to-webscrape --entry-point process_webscrape_event --stage-bucket ibc-client-activity-input-${ENVIRONMENT} --memory=${env.memory} --set-env-vars PROJECT=${env.project},CONFIG=WebScraper --update-labels=${env.labels}")
                            }
                        }
                        stage ("failed_event_service") {
                            steps {
                                sh("gcloud functions deploy failed_event_service --project ${env.project} --region=${env.region} --runtime ${env.runtime} --service-account ${env.service_account} --source=${env.source} --trigger-topic ibc.failed-events --entry-point handle_failed_event --stage-bucket ibc-client-activity-input-${ENVIRONMENT} --memory=${env.memory} --set-env-vars PROJECT=${env.project},CONFIG=WriteInboundConverterErrors --update-labels=${env.labels}")
                            }
                        }
                    }
                }
            }
        }
    }
}


from google.cloud import pubsub_v1
import os
import json

"""
PROJECTS:
tt-pd-eng
tt-temp-2021030444

"""
project_id = 'tt-pd-eng'
topic_id = 'ibc.client-activity-input'
resource_folder = 'resources/{}'.format(topic_id)
data_tag = 'attributes'

publisher = pubsub_v1.PublisherClient()
topic_path = publisher.topic_path(project_id, topic_id)


for file in os.listdir(resource_folder):
    with open(os.path.join(resource_folder, file)) as f:
        contents = json.loads(f.read())

    if data_tag == 'attributes':
        data = "Nothing".encode("utf-8")
        future = publisher.publish(topic=topic_path, data=data, **contents)

    elif data_tag == 'data':
        data = json.dumps(contents).encode("utf-8")
        future = publisher.publish(topic=topic_path, data=data)



"""
gcloud functions deploy client_activity_input --project tt-temp-2021030444 --region=us-east4 --runtime python37 --service-account tt-ibc-innovation@tt-temp-2021030444.iam.gserviceaccount.com --source=. --trigger-topic ibc.client-activity-input --entry-point process_event --stage-bucket ibc-client-activity-input-innovation --memory=128MB --set-env-vars ENV=temp-2021030444,CONFIG=InboundConverter --update-labels=tt-component=cloud-function,tt-environment=temp,tt-product=inbound-converter,tt-region=us-east4,tt-service=cloud-functions,tt-version=na,tt-zone=multi
gcloud functions deploy web_scraper --project tt-temp-2021030444 --region=us-east4 --runtime python37 --service-account tt-ibc-innovation@tt-temp-2021030444.iam.gserviceaccount.com --source=. --trigger-topic ibc.activity_urls --entry-point process_event --stage-bucket ibc-client-activity-input-innovation --memory=128MB --set-env-vars ENV=temp-2021030444,CONFIG=WebScraper --update-labels=tt-component=cloud-function,tt-environment=temp,tt-product=inbound-converter,tt-region=us-east4,tt-service=cloud-functions,tt-version=na,tt-zone=multi
gcloud functions deploy write_client_activity --project tt-temp-2021030444 --region=us-east4 --runtime python37 --service-account tt-ibc-innovation@tt-temp-2021030444.iam.gserviceaccount.com --source=. --trigger-topic ibc.complete_activities --entry-point write_to_bq --stage-bucket ibc-client-activity-input-innovation --memory=128MB --set-env-vars ENV=temp-2021030444,CONFIG=WriteInboundConverter --update-labels=tt-component=cloud-function,tt-environment=temp,tt-product=inbound-converter,tt-region=us-east4,tt-service=cloud-functions,tt-version=na,tt-zone=multi
gcloud functions deploy failed_event_service --project tt-temp-2021030444 --region=us-east4 --runtime python37 --service-account tt-ibc-innovation@tt-temp-2021030444.iam.gserviceaccount.com --source=. --trigger-topic ibc.failed-events --entry-point handle_failed_event --stage-bucket ibc-client-activity-input-innovation --memory=128MB --set-env-vars ENV=temp-2021030444,CONFIG=WriteInboundConverterErrors --update-labels=tt-component=cloud-function,tt-environment=temp,tt-product=inbound-converter,tt-region=us-east4,tt-service=cloud-functions,tt-version=na,tt-zone=multi

"""
